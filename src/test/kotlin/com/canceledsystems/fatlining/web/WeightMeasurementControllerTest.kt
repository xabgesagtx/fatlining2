package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.repository.WeighMeasurementRepository
import com.canceledsystems.fatlining.service.PAGE_SIZE
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.notNullValue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import java.math.BigDecimal

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
class WeightMeasurementControllerTest {

    @Autowired
    lateinit var webClient: WebTestClient

    @Autowired
    lateinit var repository: WeighMeasurementRepository

    @Before
    fun setUp() {
        repository.deleteAll().block()
    }

    @Test
    fun add() {
        val valueInKg = BigDecimal.valueOf(1)

        webClient.post()
                .uri {
                    it.path("/api/measurements")
                            .queryParam("valueInKg", valueInKg)
                            .build()
                }
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus()
                .isCreated
                .expectBody<WeightMeasurementDTO>()
                    .consumeWith {
                        assertThat(it.responseBody, notNullValue())
                        assertThat(it.responseBody!!.valueInKg, equalTo(valueInKg))
                    }

        webClient.get()
                .uri("/api/measurements")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody<PageDTO>()
                    .consumeWith {
                        assertThat(it.responseBody, notNullValue())
                        assertThat(it.responseBody!!.totalElements, equalTo(1L))
                    }
    }

    @Test
    fun `remove succeeds`() {
        val valueInKg = BigDecimal.valueOf(1)

        val weightMeasurement = webClient.post()
                .uri {
                    it.path("/api/measurements")
                            .queryParam("valueInKg", valueInKg)
                            .build()
                }
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus()
                .isCreated
                .expectBody<WeightMeasurementDTO>()
                .returnResult()
                .responseBody!!

        webClient.delete()
                .uri {
                    it.path("/api/measurements/{id}")
                            .build(weightMeasurement.id)
                }
                .exchange()
                .expectStatus()
                .isOk

        webClient.get()
                .uri("/api/measurements")
                .exchange()
                .expectBody<PageDTO>()
                .isEqualTo(PageDTO(number = 0,
                        size = PAGE_SIZE,
                        totalElements = 0,
                        content = emptyList()))
    }

    @Test
    fun `remove returns 404 for non existing measurement`() {
        webClient.delete()
                .uri {
                    it.path("/api/measurements/{id}")
                            .build("nonExistingId")
                }
                .exchange()
                .expectStatus()
                .isNotFound
    }

}