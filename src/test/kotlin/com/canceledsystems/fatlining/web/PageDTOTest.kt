package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.service.Page
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test

class PageDTOTest {

    @Test
    fun fromPage() {
        val actual = PageDTO.fromPage(Page(number = 0,
                size = 1,
                totalElements = 2,
                content = emptyList()))
        val expected = PageDTO(number = 0,
                size = 1,
                totalElements = 2,
                content = emptyList())
        assertThat(actual, equalTo(expected))
    }
}