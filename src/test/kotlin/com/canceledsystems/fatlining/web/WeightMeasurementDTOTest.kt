package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.service.WeightMeasurement
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant


internal class WeightMeasurementDTOTest {

    @Test
    fun `fromEntity maps all fields`() {
        val id = ""
        val valueInKg = BigDecimal.valueOf(1)
        val createdAt = Instant.now()
        val measurement = WeightMeasurement(id = id, valueInKg = valueInKg, createdAt = createdAt)

        val actual = WeightMeasurementDTO.fromMeasurement(measurement)
        val expected = WeightMeasurementDTO(id = id, valueInKg = valueInKg, createdAt = createdAt)
        MatcherAssert.assertThat(actual, Matchers.equalTo(expected))
    }

}