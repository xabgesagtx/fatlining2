package com.canceledsystems.fatlining.service

import com.canceledsystems.fatlining.repository.WeightMeasurementEntity
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant

internal class WeightMeasurementTest {

    @Test(expected = AssertionError::class)
    fun `fromEntity fails because id is null`() {
        val entity = WeightMeasurementEntity(valueInKg = BigDecimal.valueOf(1), createdAt = Instant.now())
        WeightMeasurement.fromEntity(entity)
    }

    @Test(expected = AssertionError::class)
    fun `fromEntity fails because createdAt is null`() {
        val entity = WeightMeasurementEntity(id = "", valueInKg = BigDecimal.valueOf(1))
        WeightMeasurement.fromEntity(entity)
    }

    @Test
    fun `fromEntity maps all fields`() {
        val id = ""
        val valueInKg = BigDecimal.valueOf(1)
        val createdAt = Instant.now()
        val entity = WeightMeasurementEntity(id = id, valueInKg = valueInKg, createdAt = createdAt)

        val actual = WeightMeasurement.fromEntity(entity)
        val expected = WeightMeasurement(id = id, valueInKg = valueInKg, createdAt = createdAt)
        assertThat(actual, equalTo(expected))
    }
}