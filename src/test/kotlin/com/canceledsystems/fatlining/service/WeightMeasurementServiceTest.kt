package com.canceledsystems.fatlining.service

import com.canceledsystems.fatlining.repository.WeighMeasurementRepository
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalTo
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import reactor.test.StepVerifier
import java.math.BigDecimal

@RunWith(SpringRunner::class)
@SpringBootTest
internal class WeightMeasurementServiceTest {

    @Autowired
    lateinit var service: WeightMeasurementService

    @Autowired
    lateinit var repository: WeighMeasurementRepository

    @Before
    fun setUp() {
        repository.deleteAll().block()
    }

    @Test
    fun add() {
        val valueInKg = BigDecimal.valueOf(1)
        StepVerifier.create(service.add(valueInKg))
                .assertNext {
                    assertThat(it.valueInKg, equalTo(valueInKg))
                }
                .verifyComplete()
        StepVerifier.create(service.getPage(0))
                .assertNext {
                    assertThat(it.totalElements, equalTo(1L))
                    assertThat(it.content.size, equalTo(1))
                    assertThat(it.content[0].valueInKg, equalTo(valueInKg))
                }
                .verifyComplete()

    }

    @Test
    fun remove() {
        val valueInKg = BigDecimal.valueOf(0)
        val measurement = service.add(valueInKg).block()
        StepVerifier.create(service.getPage(0))
                .assertNext {
                    assertThat(it.totalElements, equalTo(1L))
                    assertThat(it.content.size, equalTo(1))
                    assertThat(it.content[0].valueInKg, equalTo(valueInKg))
                }
                .verifyComplete()
        StepVerifier.create(service.remove(measurement!!.id))
                .verifyComplete()

        StepVerifier.create(service.getPage(0))
                .expectNext(Page(number = 0,
                        size = PAGE_SIZE,
                        totalElements = 0,
                        content = emptyList()))
                .verifyComplete()
    }

    @Test
    fun `remove non existing measurement returns with error`() {
        StepVerifier.create(service.remove("nonExistingId"))
                .verifyError(WeightMeasurementNotFound::class.java)
    }
}