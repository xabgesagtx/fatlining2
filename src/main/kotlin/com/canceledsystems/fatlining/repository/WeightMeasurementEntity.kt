package com.canceledsystems.fatlining.repository

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import java.math.BigDecimal
import java.time.Instant

data class WeightMeasurementEntity(
        @Id val id: String? = null,
        val valueInKg: BigDecimal,
        @CreatedDate val createdAt: Instant? = null
)