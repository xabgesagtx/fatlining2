package com.canceledsystems.fatlining.repository

import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface WeighMeasurementRepository: ReactiveCrudRepository<WeightMeasurementEntity, String> {
    fun findAllBy(pageRequest: PageRequest): Flux<WeightMeasurementEntity>
}