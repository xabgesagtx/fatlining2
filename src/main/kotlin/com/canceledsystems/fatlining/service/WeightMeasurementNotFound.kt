package com.canceledsystems.fatlining.service

class WeightMeasurementNotFound(id: String): RuntimeException("Measurement $id not found")