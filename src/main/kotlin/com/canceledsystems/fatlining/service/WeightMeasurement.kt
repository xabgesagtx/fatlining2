package com.canceledsystems.fatlining.service

import com.canceledsystems.fatlining.repository.WeightMeasurementEntity
import java.math.BigDecimal
import java.time.Instant

data class WeightMeasurement(
        val id: String,
        val valueInKg: BigDecimal,
        val createdAt: Instant
) {

    companion object {

        fun fromEntity(entity: WeightMeasurementEntity): WeightMeasurement {
            assert(entity.id != null)
            assert(entity.createdAt != null)
            return WeightMeasurement(
                    id = entity.id!!,
                    valueInKg = entity.valueInKg,
                    createdAt = entity.createdAt!!
            )
        }
    }
}