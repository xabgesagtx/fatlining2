package com.canceledsystems.fatlining.service

data class Page(val number: Int,
                val size: Int,
                val totalElements: Long,
                val content: List<WeightMeasurement>)