package com.canceledsystems.fatlining.service

import com.canceledsystems.fatlining.repository.WeighMeasurementRepository
import com.canceledsystems.fatlining.repository.WeightMeasurementEntity
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.math.BigDecimal

const val PAGE_SIZE = 20

@Service
class WeightMeasurementService(private val repository: WeighMeasurementRepository) {

    fun add(valueInKg: BigDecimal): Mono<WeightMeasurement> {
        val measurement = WeightMeasurementEntity(valueInKg = valueInKg)
        return repository.save(measurement)
                .map { WeightMeasurement.fromEntity(it) }
    }

    fun remove(id: String): Mono<Void> {
        return repository.findById(id)
                .switchIfEmpty(Mono.error(WeightMeasurementNotFound(id)))
                .flatMap { repository.delete(it) }
    }

    fun getPage(number: Int): Mono<Page> {
        val pageRequest = PageRequest.of(number, PAGE_SIZE)
        return repository.findAllBy(pageRequest)
                .map { WeightMeasurement.fromEntity(it) }
                .collectList()
                .flatMap { measurements ->
                    getTotalElements().map { numberOfElements ->
                        Page(number = number,
                                size = PAGE_SIZE,
                                content = measurements,
                                totalElements = numberOfElements)
                    }
                }
    }

    fun getTotalElements(): Mono<Long> {
        return repository.count()
    }

}