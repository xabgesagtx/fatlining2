package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.service.WeightMeasurementNotFound
import com.canceledsystems.fatlining.service.WeightMeasurementService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono
import java.math.BigDecimal

@RestController
@RequestMapping("api/measurements")
class WeightMeasurementController(private val service: WeightMeasurementService) {


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun add(@RequestParam valueInKg: BigDecimal): Mono<WeightMeasurementDTO> {
        return service.add(valueInKg)
                .map { WeightMeasurementDTO.fromMeasurement(it) }
    }

    @DeleteMapping("{id}")
    fun remove(@PathVariable id: String): Mono<ResponseEntity<Void>> {
        return service.remove(id)
                .map { ok().build<Void>() }
                .onErrorResume(WeightMeasurementNotFound::class.java) {
                    Mono.error(ResponseStatusException(HttpStatus.NOT_FOUND, it.message))
                }
    }

    @GetMapping
    fun getPage(@RequestParam(value = "page", defaultValue = "0") pageNumber: Int): Mono<PageDTO> {
        return service.getPage(pageNumber)
                .map((PageDTO)::fromPage)
    }
}