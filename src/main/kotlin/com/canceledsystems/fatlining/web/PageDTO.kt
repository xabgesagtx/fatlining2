package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.service.Page

data class PageDTO(val number: Int,
              val size: Int,
              val totalElements: Long,
              val content: List<WeightMeasurementDTO>) {

    companion object {
        fun fromPage(page: Page): PageDTO {
            val content = page.content.map((WeightMeasurementDTO)::fromMeasurement)
            return PageDTO(number = page.number,
                    size = page.size,
                    totalElements = page.totalElements,
                    content = content)
        }
    }

}