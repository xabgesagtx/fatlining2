package com.canceledsystems.fatlining.web

import com.canceledsystems.fatlining.service.WeightMeasurement
import java.math.BigDecimal
import java.time.Instant

data class WeightMeasurementDTO(
        val id: String,
        val valueInKg: BigDecimal,
        val createdAt: Instant
) {
    companion object {
        fun fromMeasurement(measurement: WeightMeasurement): WeightMeasurementDTO {
            return WeightMeasurementDTO(
                    id = measurement.id,
                    valueInKg = measurement.valueInKg,
                    createdAt = measurement.createdAt
            )
        }
    }
}