package com.canceledsystems.fatlining

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.config.EnableMongoAuditing

@SpringBootApplication
@EnableMongoAuditing
class FatliningApplication

fun main(args: Array<String>) {
	runApplication<FatliningApplication>(*args)
}
